<?php

namespace App\Http\Controllers;

use App\Models\Subscribe;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubscribeController extends Controller
{
    public function createSubs(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'Name' => 'required',
                'Username' => 'required|unique:subscribes,Username',
                'Password' => 'required',
                'Status' => 'required',
            ]);

            if ($validateUser->fails()) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "Fields are required",
                    "error" => $validateUser->errors()
                ], 401);
            }

            $user = Subscribe::create([
                'Name' => $request->Name,
                'Username' => $request->Username,
                'Status' => $request->Status,
                'Password' => Hash::make($request->Password),
            ]);

            return response()->json([
                "status" => 'success',
                "message" => "user created successfully",
                "token" => $user->createToken("API TOKEN")->plainTextToken
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
            ], 500);
        }
    }

    public function loginSubs(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'Username' => 'required',
                'Password' => 'required',
            ]);

            if ($validateUser->fails()) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "Fields are required",
                    "error" => $validateUser->errors()
                ], 401);
            }

            $user = Subscribe::where('Username', $request->Username)->first();
            if (!Hash::check($request->Password, $user->Password)) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "Username & Password do not match our records",
                ], 401);
            }

            if (is_null($user->device_id)) {
                $user->device_id = Str::random(60);
                $user->save();
            } elseif ($user->device_id !== $request->header('Device-Id')) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "You are already logged in from another device",
                ], 401);
            }

            return response()->json([
                "status" => 'success',
                "message" => "User logged in successfully",
                "token" => $user->createToken("API TOKEN")->plainTextToken,
                "data" => $user,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
            ], 500);
        }
    }

    public function logoutSubs(Request $request)
    {
        try {
            $user = Subscribe::where('Username', $request->Username)->first();
            if (!$user) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "User not authenticated",
                ], 401);
            }
            $user->device_id = null;
            $user->save();
            $user->tokens()->delete();
            return response()->json([
                "status" => 'success',
                "message" => "User logged out successfully",
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
            ], 500);
        }
    }

    public function getSubscribers()
    {
        try {
            $subscribers = Subscribe::all();
            if ($subscribers) {
                return response()->json([
                    "status" => 'success',
                    "data" => $subscribers,
                ], 200);
            } else {
                return response()->json([
                    "status" => 'fail',
                ], 401);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
            ], 500);
        }
    }

    public function searchNameSubscribers(Request $request)
    {
        $text = $request->input("Text");
        if (empty($text)) {
            return response()->json(["status" => "fail", "message" => "No text provided"]);
        }
        $textArray = explode(' ', $text);
        $subscribers = Subscribe::where(function ($query) use ($textArray) {
            foreach ($textArray as $text) {
                $query->orWhere('Name', 'LIKE', '%' . $text . '%');
            }
        })->get();
        if ($subscribers->count() > 0) {
            return response()->json(["status" => "success", "data" => $subscribers, "count" => $subscribers->count()]);
        } else {
            return response()->json(["status" => "fail", "message" => "No subscribers found"]);
        }
    }
    public function searchSubscribers(Request $request)
    {
        $text = $request->input("Text");
        if (empty($text)) {
            return response()->json(["status" => "fail", "message" => "No text provided"]);
        }

        $textArray = explode(' ', $text);

        $subscribers = Subscribe::where(function ($query) use ($textArray) {
            foreach ($textArray as $text) {
                $query->orWhere('Name', 'LIKE', '%' . $text . '%')
                    ->orWhere('id', 'LIKE', '%' . $text . '%')
                    ->orWhere('Username', 'LIKE', '%' . $text . '%')
                    ->orWhere('Status', 'LIKE', '%' . $text . '%');
            }
        })->get();

        if ($subscribers->count() > 0) {
            return response()->json(["status" => "success", "data" => $subscribers, "count" => $subscribers->count()]);
        } else {
            return response()->json(["status" => "fail", "message" => "No subscribers found"]);
        }
    }


    public function updateSubscriber(Request $request)
    {
        $subscriber = Subscribe::where("id", $request->Id)->first();
        if ($subscriber) {
            if ($request->has('Username') && Subscribe::where('Username', $request->Username)->where('id', '!=', $request->Id)->exists()) {
                return response()->json(["status" => "fail", "message" => "Username already exists"]);
            }
    
            // Fill the subscriber data, excluding the password for now
            $subscriber->fill($request->only(['Name', 'Username', 'Status']));
    
            // Check if the password is present in the request and hash it before saving
            if ($request->has('Password')) {
                $subscriber->Password = Hash::make($request->Password);
            }
    
            if ($subscriber->save()) {
                return response()->json(["status" => "success"]);
            } else {
                return response()->json(["status" => "fail"]);
            }
        } else {
            return response()->json(["status" => "subscriber not found"]);
        }
    }
    public function deleteSubscriber(Request $request)
    {
        $subscriber = Subscribe::where("id", $request->Id)->first();
        if ($subscriber) {
            $subscriber->delete();

            return response()->json(["status" => "success"]);
        } else {
            return response()->json(["status" => "fail"]);
        }
    }
}
