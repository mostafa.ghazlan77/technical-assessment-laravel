<?php

namespace App\Http\Controllers;

use App\Models\blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class BlogController extends Controller
{


    public function createBlog(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'Title' => 'required',
                'Image' => 'required',
                'Publish_Date' => 'required',
                'Status' => 'required',
                'Subscribers_ID' => 'required',
                'Content' => 'required',
            ]);

            if ($validateUser->fails()) {
                return response()->json([
                    "status" => 'fail',
                    "message" => "Fields are required",
                    "error" => $validateUser->errors()
                ], 401);
            }

            if ($request->hasFile('Image')) {
                $image = $request->file('Image');
                $imagename = time() . 'blog.' . $image->getClientOriginalExtension();
                $image->move(public_path('storage'), $imagename);
            } else {
                $imagename = null;
            }

            $blog = new Blog();
            $blog->Title = $request->Title;
            $blog->Publish_Date = $request->Publish_Date;
            $blog->Status = $request->Status;
            $blog->Subscribers_ID = $request->Subscribers_ID;
            $blog->Image = $imagename;
            $blog->Content = $request->Content;

            $blog->save();

            return response()->json([
                "status" => 'success',
                "message" => "Blog created successfully",
            ], 200);
        } catch (\Throwable $th) {
            // Log the error
            Log::error('Error creating blog: ' . $th->getMessage());

            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
                "error" => $th->getMessage(),
            ], 500);
        }
    }

    public function getBlog()
    {

        try {
            $blogs = blog::all();
            if ($blogs) {
                return response()->json([
                    "status" => 'success',
                    "data" => $blogs,
                ], 200);
            } else {
                return response()->json([
                    "status" => 'fail',
                ], 401);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "status" => 'fail',
                "message" => 'server error 500',
            ], 500);
        }
    }

    public function searchTitleBlogs(Request $request)
    {
        $text = $request->input("Text");
        if (empty($text)) {
            return response()->json(["status" => "fail", "message" => "No text provided"]);
        }
        $textArray = explode(' ', $text);
        $blogs = blog::where(function ($query) use ($textArray) {
            foreach ($textArray as $text) {
                $query->orWhere('Title', 'LIKE', '%' . $text . '%');
            }
        })->get();
        if ($blogs->count() > 0) {
            return response()->json(["status" => "success", "data" => $blogs, "count" => $blogs->count()]);
        } else {
            return response()->json(["status" => "fail", "message" => "No blogs found"]);
        }
    }


    public function searchBlogs(Request $request)
    {
        $text = $request->input("Text");
        if (empty($text)) {
            return response()->json(["status" => "fail", "message" => "No text provided"]);
        }

        $textArray = explode(' ', $text);

        $blogs = blog::where(function ($query) use ($textArray) {
            foreach ($textArray as $text) {
                $query->orWhere('Title', 'LIKE', '%' . $text . '%')
                    ->orWhere('id', 'LIKE', '%' . $text . '%')
                    ->orWhere('Publish_Date', 'LIKE', '%' . $text . '%')
                    ->orWhere('Status', 'LIKE', '%' . $text . '%')
                    ->orWhere('Subscribers_ID', 'LIKE', '%' . $text . '%')
                    ->orWhere('Content', 'LIKE', '%' . $text . '%');
            }
        })->get();

        if ($blogs->count() > 0) {
            return response()->json(["status" => "success", "data" => $blogs, "count" => $blogs->count()]);
        } else {
            return response()->json(["status" => "fail", "message" => "No blogs found"]);
        }
    }


    public function updateBlog(Request $request)
    {
        $blog = Blog::where("id", $request->Id)->first();
        if ($blog) {
            $oldImage = $blog->Image;
            if ($request->hasFile('Image')) {
                $image = $request->file('Image');
                $imagename = time() . 'blog.' . $image->getClientOriginalExtension();
                $image->move(public_path('storage'), $imagename);


                if ($oldImage) {
                    $oldImagePath = public_path('storage') . '/' . $oldImage;
                    if (file_exists($oldImagePath)) {
                        unlink($oldImagePath);
                    }
                }
            } else {
                $imagename = $oldImage;
            }

            $data = $request->only(['Title', 'Content', 'Status', 'Publish_Date']);
            if ($imagename) {
                $data['Image'] = $imagename;
            }

            $blog->fill($data);
            if ($blog->save()) {
                return response()->json(["status" => "success", "blog" => $blog]);
            } else {
                return response()->json(["status" => "fail"]);
            }
        } else {
            return response()->json(["status" => "blog not found"]);
        }
    }

    public function deleteBlog(Request $request)
    {
        $blog = Blog::where("id", $request->Id)->first();
        if ($blog) {
            $imageName = $blog->Image;
            $blog->delete();
            if ($imageName) {
                $imagePath = public_path('storage') . '/' . $imageName;
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            return response()->json(["status" => "success"]);
        } else {
            return response()->json(["status" => "fail"]);
        }
    }
}
