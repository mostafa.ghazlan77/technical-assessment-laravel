<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Subscribe extends Authenticatable
{
    use HasFactory, HasApiTokens;

    protected $fillable = [
        'Name',
        'Username',
        'Password',
        'Status',
    ];

    protected $hidden = [
        'Password',
    ];

    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['Password'] = bcrypt($password);
    // }
}
