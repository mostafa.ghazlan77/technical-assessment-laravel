<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\SubscribeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/




Route::post('/auth/createUser',[SubscribeController::class,'createSubs']);
Route::post('/auth/login',[SubscribeController::class,'loginSubs']);
Route::post('/auth/logout',[SubscribeController::class,'logoutSubs']);
Route::get('/getSubscribers',[SubscribeController::class,'getSubscribers']);
Route::post('/searchNameSubscribers',[SubscribeController::class,'searchNameSubscribers']);
Route::post('/searchSubscribers',[SubscribeController::class,'searchSubscribers']);
Route::post('/updateSubscriber',[SubscribeController::class,'updateSubscriber']);
Route::post('/deleteSubscriber',[SubscribeController::class,'deleteSubscriber']);

//blog
Route::post('/createBlog',[BlogController::class,'createBlog']);
Route::get('/getBlog',[BlogController::class,'getBlog']);
Route::post('/searchTitleBlogs',[BlogController::class,'searchTitleBlogs']);
Route::post('/searchBlogs',[BlogController::class,'searchBlogs']);
Route::post('/updateBlog',[BlogController::class,'updateBlog']);
Route::post('/deleteBlog',[BlogController::class,'deleteBlog']);
